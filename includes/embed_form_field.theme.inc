<?php

/**
 * Theming function used to theme fields as tokens.
 */
function theme_embed_form_field_tokens($variables) {
  $output = '';
  $fields = $variables['fields'];

  $output .= '<ul>';
  foreach ($fields as $machine_name => $field) {
    $output .= '<li>';
    $output .= '<div class="token-field-wrapper">';
    $output .= '<div class="token-field"><code>[' . $machine_name . '] &#61;&#62; ' . $field['label'] . '</code></div>';

    if (isset($field['description']) && strlen($field['description'])) {
      $output .= '<div class="token-description"><em>' . t('Description: @desc', array('@desc' => $field['description'])) . '</em></div>';
    }
    $output .= '</div>';
    $output .= '</li>';
  }
  $output .= '</ul>';
  return $output;
}

/**
 * Theming function used to theme embed form field element.
 */
function theme_embed_form_field_element($variables) {
  $element = $variables['element'];
  _form_set_class($element, array('embed-form-field-element'));

  $wrapper_attributes = array(
    'class' => array('embed-form-field-element-wrapper'),
  );
  $output = '<div' . drupal_attributes($wrapper_attributes) . '>';

  if (isset($element['#use_fieldset']) && $element['#use_fieldset']) {
    $output .= '<fieldset class="form-wrapper embed-fieldset">';
    $output .= '<legend><span class="fieldset-legend">' . $element['#fieldset'] . '</span></legend>';
    $output .= '<div class="fieldset-wrapper">';
    $output .= '<div class="embed-form-field">';
    $output .= $element['#rendered_value'];
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</fieldset>';
  }
  else {
    $output .= '<div class="embed-form-field">';
    $output .= $element['#rendered_value'];
    $output .= '</div>';
  }
  $output .= '</div>';

  return $output;
}

/**
 * Function used to output the embedded values in the form
 * of the replacement string.
 */
function theme_embed_form_field_formatter_embed_form_field_default($variables) {

  if (isset($variables['entity']) && count($variables['tokens']['replacement_tokens']) > 0) {
    $entity = $variables['entity'];
    $tokens = $variables['tokens'];
    $processing_string = $variables['field']['settings']['replacement_string'];

    foreach ($tokens['replacement_tokens'] as $index => $replacement_token) {
      $replacement_token_rendered = render(field_view_field($variables['entity_type'], $entity, $tokens['replacement_fields'][$index], array('label' => 'hidden')));
      $processing_string = str_replace($replacement_token, $replacement_token_rendered, $processing_string);
    }
    return $processing_string;
  }
}
